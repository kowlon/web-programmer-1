/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : perpustakaandemo

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 06/08/2019 12:09:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for buku
-- ----------------------------
DROP TABLE IF EXISTS `buku`;
CREATE TABLE `buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(15) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `idpengarang` int(50) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pegarang` (`idpengarang`),
  CONSTRAINT `fk_pegarang` FOREIGN KEY (`idpengarang`) REFERENCES `pengarang` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of buku
-- ----------------------------
BEGIN;
INSERT INTO `buku` VALUES (11, '9999987777', 'Harry Potret', 5, 300, '');
INSERT INTO `buku` VALUES (12, '12345', 'harry petir', 5, 111, 'Screen Shot 2019-07-15 at 13.45.05.png');
INSERT INTO `buku` VALUES (19, '112233', 'buku baru', 6, 90, 'Screen Shot 2019-08-06 at 00.37.27.png');
COMMIT;

-- ----------------------------
-- Table structure for pengarang
-- ----------------------------
DROP TABLE IF EXISTS `pengarang`;
CREATE TABLE `pengarang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengarang
-- ----------------------------
BEGIN;
INSERT INTO `pengarang` VALUES (5, 'kowlon', 'kowlon@gmail.com');
INSERT INTO `pengarang` VALUES (6, 'mqrquest', 'mqr@motogp.co');
INSERT INTO `pengarang` VALUES (7, 'tambah', 'tambah@gmail.com');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
