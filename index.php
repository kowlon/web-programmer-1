<?php
include 'config/config.php';
include 'config/function.php';
include 'fragment/header.php'; ?>

<header>
    <h1><?= APPNAME ?></h1>
</header>

<?php
include 'fragment/menu.php';
?>

<main>
    <h3>Cari Buku:</h3>
    <?= DBNAME ?>
    <?= "<br>" ?>
    <?php echo APPNAME ?>
    <form method="post">
        <div>
            <label for="judul">Judul:</label>
            <input type="text" name="judul" id="judul">
            <input type="submit" value="Cari" id="submit" name="submit">
        </div>
    </form>
    <?php
    if (isset($_POST['submit'])) {
        $judul = $_POST['judul'];
        $con = connect_db();
        $query = "SELECT * FROM buku INNER JOIN pengarang ON pengarang.id=buku.idpengarang WHERE judul LIKE '%$judul%'";
        $result = execute_query($con, $query);
        echo "<h3>Hasil Pencarian :</h3><br><br>";
        while ($data = mysqli_fetch_array($result)) {
            echo $data['judul'] . ' ( Pengarang : ' . $data['nama'] . ')<hr>';
        }
    }
    ?>
</main>
<?php
include "fragment/footer.php";
?>