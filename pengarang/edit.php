<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Edit Pengarang</h1>
</header>

<?php
include "../fragment/menu.php";
?>
<main>
    <h3></h3>
    <?php

    if (isset($_POST['submit'])) {
        $id = $_GET['id'];
        $nama = $_POST['nama'];
        $email = $_POST['email'];

        if (empty($nama)) {
            echo "nama harus diisi";
        } elseif (empty($email)) {
            echo "email harus diisi";
        } else {
            $con = connect_db();
            $query = "UPDATE pengarang SET nama='$nama',email='$email'
            WHERE id='$id'";
            $result = execute_query($con, $query);
            if (mysqli_affected_rows($con) != 0) {
                header("location:index.php");
            }
        }
    } else if (isset($_GET['id'])) {
        $con = connect_db();
        $id = $_GET['id'];
        $query = "SELECT * FROM pengarang WHERE id='$id'";
        $result = execute_query($con, $query);
        $data = mysqli_fetch_array($result);
    } else {
        header("location:index.php");
    }
    ?>

    <form name="formTambah" method="post" id="formTambah" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="<?= $data['id'] ?>" disable>
        <div>
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama" required size="50" value="<?= $data['nama'] ?>">
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" required size="30" value="<?= $data['email'] ?>">
        </div>
        <div>
            <input type="submit" value="simpan" id="submit" name="submit">
        </div>
    </form>
</main>
<?php
include "../fragment/footer.php";
?>