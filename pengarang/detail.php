<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Detail Pengarang</h1>
</header>

<?php
include "../fragment/menu.php";
?>

<main>
    <?php
    if (isset($_GET['id'])  || !empty($_GET['id'])) {
        $con = connect_db();
        $id = $_GET['id'];
        $query = "SELECT * FROM pengarang WHERE id = '$id'";
        $result = execute_query($con, $query);
        $data = mysqli_fetch_array($result);
        ?>
    <br>
    <br>
    <table border="1" width="80%">
        <tr>
            <th>Nama</th>
            <td><?= $data['nama'] ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $data['email'] ?></td>
        </tr>
    </table>
    <?php
    } else {
        header("location:index.php");
    }
    ?>
</main>
<?php
include "../fragment/footer.php";
?>