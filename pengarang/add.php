<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Tambah Pengarang</h1>
</header>

<?php
include "../fragment/menu.php";
?>
<main>
    <h3></h3>
    <?php

    if (isset($_POST['submit'])) {
        $nama = $_POST['nama'];
        $email = $_POST['email'];

        if (empty($nama)) {
            echo "nama harus diisi";
        } elseif (empty($email)) {
            echo "email harus diisi";
        } else {

            $con = connect_db();
            $query = "INSERT INTO pengarang (nama,email)"
                . "VALUES ('$nama','$email')";
            $result = execute_query($con, $query);
            if (mysqli_affected_rows($con) != 0) {
                header("location:index.php");
            }
        }
    }
    ?>

    <form name="formTambah" method="post" id="formTambah" enctype="multipart/form-data">
        <div>
            <label for="nama">Nama:</label>
            <input type="text" name="nama" id="nama" required>
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" required>
        </div>
        <div>
            <input type="submit" value="simpan" id="submit" name="submit">
        </div>
    </form>
</main>
<?php
include "../fragment/footer.php";
?>