<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
include '../fragment/menu.php';
?>

<main>
    <h3>Daftar Pengarang</h3>
    <a href="<?= BASEPATH ?>/pengarang/add.php">Tambah Pengarang</a><br><br>
    <table class="table responsive" width="50%">
        <tr align="center">
            <th>ID</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Aksi</th>
        </tr>
        <?php
        $con = connect_db();
        $query = "SELECT * FROM pengarang";
        $result = execute_query($con, $query);
        while ($data = mysqli_fetch_assoc($result)) { ?>
        <tr align="center">
            <td><?= $data['id'] ?></td>
            <td><?= $data['nama'] ?></td>
            <td><?= $data['email'] ?></td>
            <td>
                <a href="detail.php?id=<?= $data['id'] ?>"><span class="label label-info">Detail</span></a>
                <a href="edit.php?id=<?= $data['id'] ?>">Edit</a>
                <a href="delete.php?id=<?= $data['id'] ?>">Hapus</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</main>
<?php
include '../fragment/footer.php';
?>