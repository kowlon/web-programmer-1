<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Tambah Buku</h1>
</header>

<?php
include "../fragment/menu.php";
?>
<main>
    <h3></h3>
    <?php



    if (isset($_POST['submit'])) {
        $isbn = $_POST['isbn'];
        $judul = $_POST['judul'];
        $idpengarang = $_POST['idpengarang'];
        $stok = $_POST['stok'];
        $gambar = $_FILES['foto'];

        if (empty($isbn)) {
            echo "isbn harus diisi";
        } elseif (empty($judul)) {
            echo "judul buku harus diisi";
        } elseif (empty($idpengarang)) {
            echo "idpengarang harus diisi";
        } elseif (empty($stok)) {
            echo "stok harus diisi";
        } else {
            if (isset($gambar)) {

                $file_name = trim($gambar['name']);
                $file_size = $gambar['size'];
                $file_tmp = $gambar['tmp_name'];
                $file_type = $gambar['type'];
                //muncul warning di php > 7
                // $file_ext = strtolower(end((explode(".", $gambar['name']))));

                $tmp_name = explode('.', $file_name);
                $file_ext = end($tmp_name);
                $extension = array('jpeg', 'jpg', 'png');

                if (in_array($file_ext, $extension) === false) {
                    echo "File harus berupa image Jpeg atau PNG";
                } else if ($file_size > 2097152) {
                    echo "Ukuran file melebihi 2MB";
                } else {
                    move_uploaded_file($file_tmp, "../images/" . $file_name);
                }
            }

            $con = connect_db();
            $query = "INSERT INTO BUKU (isbn,judul,idpengarang,stok,gambar)"
                . "VALUES ('$isbn','$judul','$idpengarang','$stok','$file_name')";
            $result = execute_query($con, $query);
            if (mysqli_affected_rows($con) != 0) {
                header("location:index.php");
            }
        }
    }
    ?>

    <form name="formTambah" method="post" id="formTambah" enctype="multipart/form-data">
        <div>
            <label for="isbn">ISBN:</label>
            <input type="text" name="isbn" id="isbn" size="50">
        </div>
        <div>
            <label for="judul">Judul:</label>
            <input type="text" name="judul" id="judul" required size="30">
        </div>
        <div>
            <label for="foto">Foto:</label>
            <input type="file" name="foto" id="foto">
        </div>
        <div>
            <label for="pengarang">Pengarang:</label>
            <select name="idpengarang" id="idpengarang">
                <?php
                $con = connect_db();
                $query = "SELECT * FROM pengarang";
                $result = execute_query($con, $query);
                while ($pengarang = mysqli_fetch_array($result)) {
                    ?>
                <option value="<?= $pengarang['id'] ?>"><?= $pengarang['nama'] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div>
            <label for="stok">Stok:</label>
            <input type="text" name="stok" id="stok" required size="4">
        </div>
        <div>
            <input type="submit" value="simpan" id="submit" name="submit">
        </div>
    </form>
</main>
<?php
include "../fragment/footer.php";
?>