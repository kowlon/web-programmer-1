<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Detail Buku</h1>
</header>

<?php
include "../fragment/menu.php";
?>

<main>
    <?php
    if (isset($_GET['id'])  || !empty($_GET['id'])) {
        $con = connect_db();
        $id = $_GET['id'];
        $query = "SELECT * FROM buku INNER JOIN 
        pengarang ON pengarang.id=buku.idpengarang 
        WHERE buku.id = '$id'";
        $result = execute_query($con, $query);
        $data = mysqli_fetch_array($result);
        ?>
    <table>
        <tr>
            <th>ISBN</th>
            <td><?= $data['isbn'] ?></td>
        </tr>
        <tr>
            <th>Judul</th>
            <td><?= $data['judul'] ?></td>
        </tr>
        <tr>
            <th>Gambar</th>
            <td><img src="../images/<?= $data['gambar'] ?>" width="100" height="100"></td>
        </tr>
    </table>
    <?php
    } else {
        header("location:index.php");
    }
    ?>
</main>
<?php
include "../fragment/footer.php";
?>