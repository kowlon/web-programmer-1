<?php
include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
include '../fragment/menu.php';
?>

<main>
    <h3>Daftar Buku</h3>
    <a href="<?= BASEPATH ?>/buku/add.php">Tambah Buku</a><br><br>
    <table class="table responsive">
        <tr>
            <th>ISBN</th>
            <th>Judul</th>
            <th>Pengarang</th>
            <th>stok</th>
            <th>gambar</th>
            <th>Aksi</th>
        </tr>
        <?php
        $con = connect_db();
        $query = "SELECT buku.*,pengarang.nama FROM buku INNER JOIN 
        pengarang ON pengarang.id=buku.idpengarang";
        $result = execute_query($con, $query);
        //php script
        while ($data = mysqli_fetch_assoc($result)) { ?>
        <tr>
            <td><?= $data['isbn'] ?></td>
            <td><?= $data['judul'] ?></td>
            <td><?php echo $data['nama'] ?></td>
            <td><?php echo $data['stok'] ?></td>
            <td><img src="../images/<?= $data['gambar'] ?>" width="100" height="100"></td>
            <td>
                <a href="detail.php?id=<?= $data['id'] ?>">Detail</a>
                <a href="edit.php?id=<?= $data['id'] ?>">Edit</a>
                <a href="delete.php?id=<?= $data['id'] ?>">Hapus</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</main>
<?php
include '../fragment/footer.php';
?>