<?php

include '../config/config.php';
include '../config/function.php';
include '../fragment/header.php';
?>

<header>
    <h1>Edit Buku</h1>
</header>

<?php
include "../fragment/menu.php";
?>
<main>
    <h3></h3>
    <?php

    if (isset($_POST['submit'])) {
        $id = $_GET['id'];
        $isbn = $_POST['isbn'];
        $judul = $_POST['judul'];
        $idpengarang = $_POST['idpengarang'];
        $stok = $_POST['stok'];
        $gambar = $_FILES['foto'];

        if (empty($isbn)) {
            echo "isbn harus diisi";
        } elseif (empty($judul)) {
            echo "judul buku harus diisi";
        } elseif (empty($idpengarang)) {
            echo "idpengarang harus diisi";
        } elseif (empty($stok)) {
            echo "stok harus diisi";
        } else {
            if (isset($gambar)) {
                $file_name = trim($gambar['name']);
                $file_size = $gambar['size'];
                $file_tmp = $gambar['tmp_name'];
                $file_type = $gambar['type'];
                //muncul warning di php > 7
                // $file_ext = strtolower(end((explode(".", $gambar['name']))));

                $tmp_name = explode('.', $file_name);
                $file_ext = end($tmp_name);
                $extension = array('jpeg', 'jpg', 'png');

                if (in_array($file_ext, $extension) === false) {
                    echo "File harus berupa image Jpeg atau PNG";
                } else if ($file_size > 2097152) {
                    echo "Ukuran file melebihi 2MB";
                } else {
                    move_uploaded_file($file_tmp, "../images/" . $file_name);
                }
            }

            $con = connect_db();
            $query = "UPDATE buku SET isbn='$isbn',judul='$judul',idpengarang='$idpengarang',stok='$stok',gambar='$file_name' WHERE id='$id'";
            $result = execute_query($con, $query);
            if (mysqli_affected_rows($con) != 0) {
                header("location:index.php");
            }
        }
    } else if (isset($_GET['id'])) {
        $con = connect_db();
        $id = $_GET['id'];
        $query = "SELECT * FROM buku WHERE id='$id'";
        $result = execute_query($con, $query);
        $data = mysqli_fetch_array($result);
    } else {
        header("location:index.php");
    }
    ?>

    <form name="formTambah" method="post" id="formTambah" enctype="multipart/form-data">
        <input type="text" name="id" id="id" value="<?= $data['id'] ?>" disable>
        <div>
            <label for="isbn">ISBN:</label>
            <input type="text" name="isbn" id="isbn" required size="50" value="<?= $data['isbn'] ?>">
        </div>
        <div>
            <label for="judul">Judul:</label>
            <input type="text" name="judul" id="judul" required size="30" value="<?= $data['judul'] ?>">
        </div>
        <div>
            <label for="foto">Foto:</label>
            <input type="file" name="foto" id="foto">
            <img src="<?= BASEPATH . '/images/' . $data['gambar'] ?>" width="100" height="100">
        </div>
        <div>
            <label for="pengarang">Pengarang:</label>
            <select name="idpengarang" id="idpengarang">
                <?php
                $con = connect_db();
                $query = "SELECT * FROM pengarang";
                $result = execute_query($con, $query);
                while ($pengarang = mysqli_fetch_array($result)) {
                    //if ternary operator
                    $selected = $pengarang['id'] == $data['idpengarang'] ? "selected" : "";

                    ?>
                <option $selected value=" <?= $pengarang['id'] ?>"><?= $pengarang['nama'] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div>
            <label for="stok">Stok:</label>
            <input type="text" name="stok" id="stok" required size="4" value="<?= $data['stok'] ?>">
        </div>
        <div>
            <input type="submit" value="simpan" id="submit" name="submit">
        </div>
    </form>
</main>
<?php
include "../fragment/footer.php";
?>